/* eslint-disable @typescript-eslint/no-var-requires */

/* eslint-disable no-undef */
import { build } from 'esbuild';
import { readdirSync, rmSync, statSync } from 'fs';
import { join } from 'path';
import pkg from './package.json' assert { type: 'json' };

let fileArray = [];
const getFilesRecursively = (dir) => {
    const files = readdirSync(dir);
    files.forEach((file) => {
        const filePath = join(dir, file);
        if (statSync(filePath).isDirectory()) {
            getFilesRecursively(filePath);
        } else {
            fileArray.push(filePath);
        }
    });
};

getFilesRecursively('src');

// remove dist folder
rmSync('dist', { recursive: true, force: true });

const entryPoints = fileArray.filter((file) => file.endsWith('.ts'));

build({
    entryPoints,
    logLevel: 'info',
    outdir: 'dist',
    bundle: true,
    platform: 'node',
    format: 'cjs',
    minify: true,
    sourcemap: false,
    external: [...Object.keys(pkg.dependencies || {}), ...Object.keys(pkg.peerDependencies || {})],
});
