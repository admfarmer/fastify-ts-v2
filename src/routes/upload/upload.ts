import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { Knex } from 'knex';
import fs from 'node:fs';
import path from 'node:path';

import { FileModel } from '../../models/file';

export default async (fastify: FastifyInstance): Promise<void> => {
    const db = fastify.db as Knex;
    const fileModel = new FileModel();
    // Set database connection
    fileModel.setConnection(db);

    // Default upload path
    const UPLOAD_PATH = process.env.UPLOAD_PATH || './uploads';
    // Default tmp path
    const TMP_PATH = process.env.TMP_PATH || './tmp';

    // Create upload directroy if not exists
    if (!fs.existsSync(UPLOAD_PATH)) {
        fs.mkdirSync(UPLOAD_PATH);
    }

    // Create tmp directroy if not exists
    if (!fs.existsSync(TMP_PATH)) {
        fs.mkdirSync(TMP_PATH);
    }

    fastify.post('/', async function (request: FastifyRequest, reply: FastifyReply) {
        const files = await request.saveRequestFiles({ tmpdir: TMP_PATH });
        // console.log(files);

        const userId = request.jwtDecoded.sub;

        // Check if file uploaded
        if (files.length === 0) {
            return reply.status(400).send({
                ok: false,
                statusCode: 400,
                error: 'No files uploaded.',
                reqId: request.id,
            });
        }

        try {
            const fileName = files[0].filename;
            const fileType = files[0].mimetype;

            // Generate new file name
            const fileExtension = fileName.split('.').pop();
            const randomUuid = crypto.randomUUID();
            const newFileName = `${randomUuid}.${fileExtension}`;
            const newFilePath = path.join(UPLOAD_PATH, newFileName);

            // get file buffer
            const fileBuffer = fs.readFileSync(files[0].filepath);
            // Create file from buffer
            fs.writeFileSync(newFilePath, fileBuffer);

            // Save to database
            fileModel.fileName = fileName;
            fileModel.type = fileType;
            fileModel.path = newFileName;
            fileModel.userId = userId;
            await fileModel.save();
            // Send response to client
            return reply.send({ ok: true, statusCode: 200, reqId: request.id });
        } catch (error) {
            // Set logging
            request.log.error({
                message: JSON.stringify(error),
                reqId: request.id,
                module: 'FILE',
            });

            // Send error
            return reply.status(500).send({
                ok: false,
                statusCode: 10500,
                error: 'Internal server error.',
                reqId: request.id,
            });
        }
    });
};
