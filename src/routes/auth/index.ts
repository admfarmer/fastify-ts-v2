import { FastifyInstance } from 'fastify';

import login from './login';
import logout from './logout';

export default async (fastify: FastifyInstance): Promise<void> => {
    fastify.register(login);
    fastify.register(logout);
};
