import closeWithGrace from 'close-with-grace';
import Fastify, { FastifyInstance, FastifyLoggerOptions, FastifyRequest } from 'fastify';
import { PinoLoggerOptions } from 'fastify/types/logger';
import FileStreamRotator from 'file-stream-rotator/lib/FileStreamRotator';

import appService from './app';

const prdLogConfig: FastifyLoggerOptions & PinoLoggerOptions = {
    redact: ['headers.authorization'],
    stream: FileStreamRotator.getStream({
        filename: './logs/app-%DATE%',
        extension: '.log',
        audit_file: './logs/audit.json',
        frequency: 'daily',
        date_format: 'YYYY-MM-DD',
        size: '10M',
        max_logs: '10',
    }),
};

const devLogConfig: FastifyLoggerOptions & PinoLoggerOptions = {
    transport: {
        target: 'pino-pretty',
        options: {
            translateTime: 'HH:MM:ss Z',
            ignore: 'pid,hostname',
        },
    },
    serializers: {
        req(request: FastifyRequest) {
            return {
                method: request.method,
                url: request.url,
                path: request.routerPath,
                parameters: request.params,
                headers: request.headers,
            };
        },
    },
};

// Instantiate Fastify with some config
const app = Fastify({
    requestIdLogLabel: 'x-track-id',
    genReqId: function (req) {
        return crypto.randomUUID();
    },
    logger: process.env.NODE_ENV === 'production' ? prdLogConfig : devLogConfig,
    disableRequestLogging: true,
    exposeHeadRoutes: false,
    // Setting reversed proxy
    // trustProxy: ['127.0.0.1', '192.168.1.1/24'],
});

app.addHook('onRequest', (request, reply, done) => {
    request.log.info({
        message: 'request received',
        reqId: request.id,
        hostname: request.hostname,
        remoteAddress: request.headers['x-real-ip'] || request.ip,
        remotePort: request.socket.remotePort,
        url: request.raw.url,
        method: request.method,
        parameters: request.params,
        headers: request.headers,
    });
    done();
});

app.addHook('onResponse', (request: FastifyRequest, reply, done) => {
    request.log.info({
        message: 'request completed',
        url: request.raw.url,
        statusCode: reply.raw.statusCode,
        reqId: request.id,
    });
    done();
});

// Register your application as a normal plugin.
app.register(appService);

// delay is the number of milliseconds for the graceful close to finish
const closeListeners = closeWithGrace({ delay: 500 }, async function ({ signal, err }) {
    if (err) {
        app.log.error(err);
    }

    app.log.info(`${signal} received, server closing`);
    await app.close();
});

app.addHook('onClose', (_instance: FastifyInstance, done: () => void) => {
    closeListeners.uninstall();
    done();
});

// Error handler
app.setErrorHandler(function (error, request, reply) {
    request.log.error(error);
    if (error.statusCode === 429) {
        reply.code(429);
        // Ratelimit error message
        error.message = 'Too many requests. Please try again later.';
    }

    reply.send({ ok: false, statusCode: error.statusCode, error: error.message });
});

// 404 handler
app.setNotFoundHandler(function (request, reply) {
    reply.status(404).send({ ok: false, statusCode: 404, error: 'Not found' });
});

// Start listening.
app.listen(
    {
        port: Number(process.env.PORT) || 3000,
        host: '0.0.0.0',
    },
    (err) => {
        if (err) {
            app.log.error(err);
            process.exit(1);
        }
    }
);

app.ready(() => {
    if (process.env.NODE_ENV !== 'production') {
        console.log(app.printRoutes({ commonPrefix: false }));
    }
});
