import { FastifyRequest } from 'fastify';
import fp from 'fastify-plugin';

import FastifyRateLimit from '@fastify/rate-limit';

export default fp(async (fastify) => {
    fastify.register(FastifyRateLimit, {
        global: true,
        max: Number(process.env.MAX_RATE_LIMIT || '100'),
        timeWindow: '1 minute',
        keyGenerator: (request: FastifyRequest) => {
            const xRealIp = request.headers['x-real-ip'];
            return xRealIp ? xRealIp.toString() : '';
        },
        errorResponseBuilder: () => {
            return {
                statusCode: 429,
                message: 'Too many requests. Please try again later.',
            };
        },
        addHeadersOnExceeding: {
            'x-ratelimit-limit': false,
            'x-ratelimit-remaining': false,
            'x-ratelimit-reset': false,
        },
        addHeaders: {
            'x-ratelimit-limit': false,
            'x-ratelimit-remaining': false,
            'x-ratelimit-reset': false,
            'retry-after': false,
        },
    });
});
