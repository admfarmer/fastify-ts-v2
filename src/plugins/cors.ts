import { FastifyRequest } from 'fastify';
import fp from 'fastify-plugin';

import cors from '@fastify/cors';

export default fp(async (fastify) => {
    fastify.register(cors, () => {
        return (request: FastifyRequest, done: any) => {
            const corsOptions = {
                origin: true,
            };

            if (/^localhost$/m.test(request.headers.origin as string)) {
                corsOptions.origin = false;
            }

            done(null, corsOptions);
        };
    });
});
